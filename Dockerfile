FROM keisukesatomi/composer:latest

COPY ./start.sh /root/start.sh

RUN chmod ugo+x /root/start.sh

ENTRYPOINT ["/root/start.sh"]
