#!/bin/bash
set -u

# download repository to tmp directory once. and then rsync it to target directory.
# deploy to where
if [ -v DEPLOY_TO_DIR ] && [ ${#DEPLOY_TO_DIR} -gt 1 ]; then
  echo "DEPLOY_TO_DIR is set"
else
  echo "DEPLOY_TO_DIR is not set"
  exit;
fi

# target branch
if [ -v TARGET_BRANCH ] && [ ${#TARGET_BRANCH} -gt 1 ]; then
  echo "TARGET_BRANCH is set"
else
  echo "TARGET_BRANCH is not set"
  exit;
fi

# TARGET_GIT_REP
if [ -v TARGET_GIT_REP ] && [ ${#TARGET_GIT_REP} -gt 1 ]; then
  echo "TARGET_GIT_REP is set"
else
  echo "TARGET_GIT_REP is not set"
  exit;
fi


# make tmp directory
DEPLOY_TMP_DIR=/var/deployphp/tmp
if [ -e ${DEPLOY_TMP_DIR} ]; then
  echo "something exists in ${DEPLOY_TMP_DIR}. try to remove"
  rm -rf ${DEPLOY_TMP_DIR}/* ${DEPLOY_TMP_DIR}/.[^.] ${DEPLOY_TMP_DIR}/.??*
else
  mkdir -p ${DEPLOY_TMP_DIR}
fi

chmod -R 777 ${DEPLOY_TMP_DIR}

# download tmp dir
git clone --depth 1 -b ${TARGET_BRANCH} ${TARGET_GIT_REP} ${DEPLOY_TMP_DIR}

echo "clone finished"
# composer and submodule setup
cd ${DEPLOY_TMP_DIR}

# git submodule
git submodule init

echo "submodule init finished"
git submodule update

echo "submodule update finished"

# composer if needed
if [ -e composer.json -o -e composer.lock ]; then
  echo "composer file exists"
  composer install
else
  echo "no composer.json nor composer.lock"
fi

# exclude of rsync parameter
if [ -v RSYNC_EXCLUDE ] && [ ${#RSYNC_EXCLUDE} -gt 1 ]; then
    echo "exclude is set"
    RSYNC_EXCLUDE="--exclude=${RSYNC_EXCLUDE}"
else
    echo "no exclude parameter"
    RSYNC_EXCLUDE=""
fi

if [ -v DRY_RUN ] && [ ${DRY_RUN} -eq 1 ]; then
  echo "dry run"
  # rsync to target directory
  rsync -ax --delete --dry-run --stats --exclude=".git" --exclude=".gitignore" ${RSYNC_EXCLUDE} ${DEPLOY_TMP_DIR}/ ${DEPLOY_TO_DIR}
else
  # rsync to target directory
  rsync -ax --delete --stats --exclude=".git" --exclude=".gitignore" ${RSYNC_EXCLUDE} ${DEPLOY_TMP_DIR}/ ${DEPLOY_TO_DIR}
fi
